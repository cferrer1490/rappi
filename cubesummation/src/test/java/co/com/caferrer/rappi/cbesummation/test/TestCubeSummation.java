package co.com.caferrer.rappi.cbesummation.test;

import java.util.HashMap;
import java.util.Map;

import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.specification.RequestSpecification;

import static com.jayway.restassured.RestAssured.given;;

public class TestCubeSummation   {


	@BeforeTest
	public void init() {
		
        RestAssured.port=8080;

        String basePath = "cubesummision/rest/";
        RestAssured.basePath = basePath;
        RestAssured.baseURI = "http://localhost";

		
	}
	
	
	@Test
	public void testUpdate() {
		   
		Map<String,String> car = new HashMap<>();
        car.put("x", "1");
        car.put("y", "1");
        car.put("z", "1");
        car.put("w", "4");
        
        //verifica que llegue el OK del json de respeusta
        given()
        .contentType("application/json")
        .body(car)
        .when().post("cube/update/1").then().body(Matchers.containsString("OK"))
        .statusCode(200);
        
        
        

	}
	
	
	
}
